
# Curso Programación Backend 32085

En este repositorio se encontraran con todo el código fuente de las clases y desafios para la comisón 32085 en Coder House. 

[comment]: <> (This is a comment, it will not be included)

## Installation

Install face-recognition-poc with npm

```bash
  git clone https://gitlab.com/kevjooeworld/programacion_backend32085
  cd programacion_backend32085
```
    
## Authors
Kevin J. Oliva [@kevjooeworld](https://github.com/kevjooeworld)

