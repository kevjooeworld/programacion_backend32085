const fs = require('fs/promises');

class Books {
    constructor(ruta){
        this.ruta = ruta
    }

    async listarAll(){
        try {
            const objs = await fs.readFile(this.ruta, 'utf-8');
            return JSON.parse(objs);
        } catch (error) {
            return []
        }
    }

    async guardar(obj){
        try {
            const objs = await this.listarAll();

            let newId;
            if (objs.length == 0) {
                newId = 1
            } else {
                newId = objs[objs.length - 1].id + 1
            }
    
            const newObj = {id: newId, ...obj}
            objs.push(newObj);
    
            await fs.writeFile(this.ruta, JSON.stringify(objs, null, 2));
            return newId;
        } catch (error) {
            console.log('error al guardar')
        }
    }

    async actualizar(id, newObj){
        try {
            const objs = await this.listarAll();
            const indexObj = objs.findIndex((o)=> o.id == id);
            // const oldObj = objs.find((o)=> o.id == id);//para buscar un objeto en un array

            if (indexObj == -1) {
                return 'Objeto no encontrado'
            } else {
                objs[indexObj] = {id, ...newObj};
                await fs.writeFile(this.ruta, JSON.stringify(objs, null, 2));
            }
            return {id, ...newObj};
        } catch (error) {
            console.log('error al actualziar')
        }
    }

    async eliminar(id){
        try {
            const objs = await this.listarAll();
            const indexObj = objs.findIndex((o)=> o.id == id);

            if (indexObj == -1) {
                return 'Elemento no encontrado'
            } else {
                objs.splice(indexObj, 1);
                await fs.writeFile(this.ruta, JSON.stringify(objs, null, 2)); 
            }
        } catch (error) {
            return 'No se pudo eliminar'
        }
    }
}

async function main(){
    const books = new Books('./DB/books-data.txt');

    console.log('Consulta inicial')
    console.log(await books.listarAll())
    console.log('Guarda obejetos')
    console.log(await books.guardar({title: "Harry Potter", author: "J. K. Rowling"}))
    console.log(await books.guardar({title: "Harry Potter", author: "J. K. Rowling"}))
    console.log('Actualiza objeto')
    console.log(await books.actualizar(2, {"title": "Lord of the Rings", "author": "J. R. R. Tolkien."}));
    console.log('Consulta objetos')
    console.log(await books.listarAll())
    console.log('Elimina objeto')
    console.log(await books.eliminar(1));
    console.log('Consulta objetos')
    console.log(await books.listarAll());
    console.log('Guarda obejetos')
    console.log(await books.guardar({title: "Harry Potter", author: "J. K. Rowling"}));
    console.log('Consulta objetos')
    console.log(await books.listarAll());
}
main();