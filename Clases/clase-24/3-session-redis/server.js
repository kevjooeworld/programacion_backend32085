/* ---------------------- Modulos ----------------------*/
import express from "express";
import session from "express-session";
import dotenv from 'dotenv';
dotenv.config();

//session persistencia redis
import Redis from "ioredis";
import connectRedis from 'connect-redis';
const RedisStore = connectRedis(session);

const app = express();

/* ---------------------- Middlewares---------------------- */
app.use(express.json());

//conf redis cli
const client = new Redis({
    host: 'localhost',
    port: 6379
})

//Session Setup
app.use(session({
    store: new RedisStore({
        client: client,
        ttl: 120,
        retries: 0
    }),
    secret: process.env.SECRET_KEY,
    resave: true,
    saveUninitialized: true
}))
// Session Middleware
function auth(req, res, next) {
    if (req.session?.user && req.session?.admin) {
      return next()
    }
    return res.status(401).send('error de autorización!')
}

/* ---------------------- Routes ---------------------- */
app.get('/con-session', (req, res)=> {
    if (!req.session.contador) {
        req.session.contador =1;
        res.send('Bienvenid@, primer login!');
    } else {
        req.session.contador++;
        res.send(`Ud ha visitado el sitio ${req.session.contador} veces`)
    }
});

app.get('/info', (req, res)=> {
    res.send(req.sessionID);
});

//Simulando un login
app.get('/login', (req, res) => {
    const { username, password } = req.query
    if (username !== 'pepe' || password !== 'pepepass') {
      return res.send('login failed')
    }

    req.session.user = username;
    req.session.admin = true;
    
    res.send('login success!')
})

app.get('/logout', (req, res)=> {
    req.session.destroy(err=>{
        if (err) {
            res.json({err});
        } else {
            res.send('Logout ok!');
        }
    });
});

app.get('/privado', auth, (req, res) => {
    res.send('si estas viendo esto es porque ya te logueaste!')
})

/* ---------------------- Server ---------------------- */
const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`Servidor express escuchando en el puerto ${PORT}`);
});