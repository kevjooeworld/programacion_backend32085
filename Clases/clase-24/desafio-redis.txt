127.0.0.1:6379> KEYS *
(empty array)
127.0.0.1:6379> SET producto1 "Quadcast S"
OK
127.0.0.1:6379> SET producto2 "Bicicleta"
OK
127.0.0.1:6379> SET producto3 "Celular"
OK
127.0.0.1:6379> SET producto4 "Televisor"
OK
127.0.0.1:6379> SET producto5 "Gabinete"
OK
127.0.0.1:6379> KEYS *
1) "producto4"
2) "producto1"
3) "producto2"
4) "producto3"
5) "producto5"
127.0.0.1:6379> GET producto1
"Quadcast S"
127.0.0.1:6379> GET producto2
"Bicicleta"
127.0.0.1:6379> GET producto3
"Celular"
127.0.0.1:6379> GET producto4
"Televisor"
127.0.0.1:6379> GET producto5
"Gabinete"
127.0.0.1:6379> SET producto6 "Mouse" EX 30
OK
127.0.0.1:6379> ttl producto6
(integer) 27
127.0.0.1:6379> GET producto6
"Mouse"
127.0.0.1:6379> ttl producto6
(integer) 12
127.0.0.1:6379> ttl producto6
(integer) 11
127.0.0.1:6379> ttl producto6
(integer) 8
127.0.0.1:6379> ttl producto6
(integer) 3
127.0.0.1:6379> KEYS *
1) "producto4"
2) "producto1"
3) "producto2"
4) "producto3"
5) "producto5"
127.0.0.1:6379>