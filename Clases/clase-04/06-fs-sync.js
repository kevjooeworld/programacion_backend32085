const fs = require('fs');

let fechaHora = new Date();

try {
    /*=================== [Lectura de archivos]*/
    const data1 = fs.readFileSync('./archivos/sync_data.txt', 'utf-8');
    console.log(data1);

    /*=================== [Escritura de archivos]*/
    fs.writeFileSync('./archivos/sync_data.txt', `Hola Coders de todo el mundo! ${fechaHora}`);

    /*=================== [Agregar contenido de archivos]*/
    fs.appendFileSync('./archivos/sync_data.txt', ' Es una buena noche para aprender :D');

    const data2 = fs.readFileSync('./archivos/sync_data.txt', 'utf-8');
    console.log('Lectura 2:', data2);
} catch (error) {
    console.log('Error: ', error);
}