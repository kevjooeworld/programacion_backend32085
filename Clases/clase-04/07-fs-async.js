const fs = require('fs')

console.log('Inicia la ejecucion')
let fechaHora = new Date();

try {
    /*=================== [Lectura de archivos]*/
    fs.readFile('./archivos/sync_data.txt', 'utf-8', (error, contenido) => {
        if (error) {
            console.log('Error: ', error);
        } else {
           console.log('Contenido: ', contenido); 

            /*=================== [Escritura de archivos]*/
            fs.writeFile('./archivos/async_data.txt', 'Sobre escribe el contenido', error => {
                if (error) {
                    throw new Error(error);
                } else {
                    console.log('Finalizado!')
                }
            })
        }
    });

    console.log('Otra instruccion')

} catch (error) {
    
}
console.log('Fin la ejecucion')