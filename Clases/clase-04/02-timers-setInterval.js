console.log(`11111 Inicio de ejecucion ${new Date()}`);

let contador = 0;

let procesoRecursivo = setInterval(()=>{
    contador++;
    console.log(`22222 Se ejecuta ${contador} veces`)

    if (contador == 5) {
        clearInterval(procesoRecursivo);
        console.log(`22222 Fin de Interval con ${contador} ejecuciones`)
    }
}, 1000);

console.log(`33333 Fin de ejecucion ${new Date()}`);