const fs = require('fs');

try {
    fs.readFile('./archivos/package.json', 'utf-8', (error, contenido) => {
        if (error) {
            throw new Error(error);
        } else {
            
            const info = {
                contenidoStr: JSON.stringify(contenido),
                contenidoObj: JSON.parse(contenido),
                autor: ''
            }
            console.log(info);

            fs.writeFile('./archivos/info.txt', JSON.stringify(info, null, 2), error => {
                if (error) {
                    throw new Error(error);
                } else {
                    console.log('Finalizado!')
                }
            })
        }
    })
} catch (error) {
    console.error(error);
}