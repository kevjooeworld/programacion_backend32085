const fs = require('fs/promises');
const ruta = './archivos/desafio-data.json'

async function listarAll() {
    try {
        const objs = await fs.readFile(ruta, 'utf-8')

        return JSON.parse(objs)
    } catch (error) {
        return []
    }
}

async function guardar(obj) {
    const objs = await listarAll()

    let newId
    if (objs.length == 0) {
        newId = 1
    } else {
        newId = objs[objs.length - 1].id + 1
    }

    const newObj = { ...obj, id: newId }
    objs.push(newObj)

    try {
        await fs.writeFile(ruta, JSON.stringify(objs, null, 2))
        return newObj;
    } catch (error) {
        throw new Error(`Error al guardar: ${error}`)
    }
}

async function main() {
    console.log(await listarAll())
    await guardar({
        "producto": "Monitor",
        "precio": 600 
    })
    console.log(await listarAll())
}
main();