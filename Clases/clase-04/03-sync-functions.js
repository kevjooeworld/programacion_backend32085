/*
    Function "delay": simula una tarea que toma un breve tiempo de ejecucion
    function "hacerTarea": Ejecuta un proceso que depende de la tarea "delay"
*/

const delay = ret => {for(let i=0; i<ret*3e6; i++);}

function hacerTarea(num) {
    console.log('haciendo tarea ' + num)
    delay(100)
    console.log(cont)
}

console.log('inicio de tareas');
hacerTarea(1)
hacerTarea(2)
hacerTarea(3)
hacerTarea(4)
console.log('fin de tareas')
console.log('otras tareas ...')