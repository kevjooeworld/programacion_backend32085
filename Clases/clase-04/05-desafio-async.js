const mostrarLetras = (palabra, tiempoDemora, callback) => {
    let contador = 0;
    let iteracion = setInterval(() => {
        console.log(palabra.charAt(contador));
        contador += 1;
        if (contador === palabra.length){
            clearInterval(iteracion);
            callback();
        }
    }, tiempoDemora);
    
}

mostrarLetras('¡Hola!', 0, () => console.log('terminé ¡Hola!'));
mostrarLetras('******', 1000, () => console.log('terminé ******'));
mostrarLetras('@@@@@@', 2000, () => console.log('terminé @@@@@@'));