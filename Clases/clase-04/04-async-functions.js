/*
    "setTimeout": Ejecuta el callback luego de 100 ms
    function "hacerTarea": Ejecuta un proceso que depende de la tarea "delay"
*/

function hacerTarea(num, cb) {
    console.log('haciendo tarea ' + num)
    setTimeout(cb,100)
}

function ejecutar(proceso) {
    console.log(`Se ejecuta el proceso ${proceso}`)
}

console.log('inicio de tareas async');
hacerTarea(1, () => {
    hacerTarea(2, () => {
        hacerTarea(3, () => {
            hacerTarea(4, () => {
                console.log('fin de tareas async')
            })
        })
    })
})
console.log('otras tareas ...')
ejecutar('Proceso sync')