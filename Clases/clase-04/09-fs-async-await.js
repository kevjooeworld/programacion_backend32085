const fs = require('fs');

async function main(params) {
    try {
        const data1 = await fs.promises.readFile('./archivos/async_data.txt', 'utf-8');
        console.log(data1);

        await fs.promises.writeFile('./archivos/async_data.txt', 'Se sobre escribe (await)')

        const data2 = await fs.promises.readFile('./archivos/async_data.txt', 'utf-8');
        console.log(data2)       
         
    } catch (error) {
        console.log(error)
    }
}
main();