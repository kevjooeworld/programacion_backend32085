const express = require('express');

const app = express();

//Middleware
app.use(express.json());

const DB_ARTISTAS = [
    {
        id: 'asdasdasda',
        name: 'Mac Miller',
        spotify: 'https://open.spotify.com/artist/4LLpKhyESsyAXpc4laK94U?si=NsJg2PZjQOmcKFAKveVLYQ'
    }
];

app.get('/api/artistas', (req, res)=>{
    res.status(200).json(DB_ARTISTAS);
});

app.get('/api/artistas/:id', (req, res)=>{
    try {
        const id = req.params.id;

        const indexObj = DB_ARTISTAS.findIndex((o)=> o.id == id);

        if (indexObj == -1) {
            res.status(404).json({code: 404, msg: `Artista ${id} no encontrado`})
        } 
        res.status(200).json(DB_ARTISTAS[indexObj]);
    } catch (error) {
        console.log(error)
        res.status(500).json({code: 500, msg: `Error al obtener ${req.method} ${req.url}`});
    }
});

app.post('/api/artistas', (req, res)=>{
    const {id, name, spotify } = req.body;
    DB_ARTISTAS.push(req.body);
    res.status(201).json({code: 201, msg: `Artista ${name} guardado con exito`});
});

app.get('*', (req, res)=>{
    /* Returning a 404 status code. */
    res.status(404).json({
        code: 404,
        msg: 'not found'
    })
});

const PORT = 3000;
const server = app.listen(PORT, ()=> {
    console.log(`Servidor escuchando en el puerto ${server.address().port}`)
});
server.on('error', err => console.log(`Error en el servidor ${err}`));