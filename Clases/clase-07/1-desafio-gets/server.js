const express = require('express');

const app = express();

const frase = "Hola Coders como están?!"

app.get('/api/frase', (req, res)=>{
    res.status(200).send(frase);
})

app.get('/api/letras/:num', (req, res)=>{
    const num = parseInt(req.params.num);

    if (isNaN(num)) {
        return res.status(400).send({error: 'El parametro ingresado no es un numero'})
    }

    if (num < 1 || num > frase.length) {
        return res.status(400).send({ error: 'El parámetro está fuera de rango' })
    }

    res.status(200).send(frase[num - 1]);
})

app.get('/api/palabras/:num', (req, res)=>{
    const num = parseInt(req.params.num);

    if (isNaN(num)) {
        return res.status(400).send({error: 'El parametro ingresado no es un numero'})
    }

    const palabras = frase.split(' ');
    if (num < 1 || num > palabras.length) {
        return res.status(400).send({ error: 'El parámetro está fuera de rango' })
    }

    res.status(200).send(palabras[num - 1]);
})

app.get('*', (req, res)=>{
    /* Returning a 404 status code. */
    res.status(404).json({
        code: 404,
        msg: 'not found'
    })
})

const PORT = 8080;
const server = app.listen(PORT, ()=> {
    console.log(`Servidor escuchando en el puerto ${server.address().port}`)
});
server.on('error', err => console.log(`Error en el servidor ${err}`));