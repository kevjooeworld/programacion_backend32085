/* ---------------------- Modulos ----------------------*/
import express from 'express';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
dotenv.config();

/* ---------------------- Instancia server ---------------------- */
const app = express();

/* ---------------------- Middlewares---------------------- */
app.use(cookieParser(`${process.env.SECRET_KEY}`));

/* ---------------------- Routes ---------------------- */
app.get('/setCookie', (req, res)=>{
    res.cookie('normalCookie', 'valorCookienueva456').send(`Cookie guardada`);
})

app.get('/setCookieVolatil', (req, res)=>{
    res.cookie('cookieVolatil', 'valorCookienueva987', {maxAge: 10000}).send(`Cookie guardada`);
})

app.get('/setSignedCookie', (req, res)=>{
    res.cookie('signedCookie', 'valorCookienueva456', {signed: true}).send(`Cookie guardada`);
})

app.get('/getCookies', (req, res)=>{
    res.send({cookies: req.cookies, signedCookies: req.signedCookies});
})

app.get('/clrCookie/:cookieName', (req, res)=>{
    res.clearCookie(req.params.cookieName).send('Cookie eliminada')
})

/* ---------------------- Server ---------------------- */
const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`Servidor express escuchando en el puerto ${PORT}`);
});