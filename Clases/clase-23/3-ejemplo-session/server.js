/* ---------------------- Modulos ----------------------*/
import express from "express";
import session from "express-session";
import dotenv from 'dotenv';
import e from "express";
dotenv.config();

const app = express();

/* ---------------------- Middlewares ---------------------- */
app.use(session({
  secret: process.env.SECRET_KEY,
  resave: true,
  saveUninitialized: true
}));

function auth(req, res, next) {
  if(req.session?.user === 'coder' && req.session?.admin){
    return next()
  }

  return res.status(401).send('Forbbiden access')
}

/* ---------------------- Routes ---------------------- */
let contador = 1;
app.get('/sin-session', (req, res)=>{
  res.json({contador: contador++});
})

app.get('/con-session', (req, res)=>{
  if (!req.session.contador) {
    req.session.contador = 1
    res.send("Bienvenido primer login");
  } else {
    req.session.contador++
    res.send(`Usted ha ingresado ${req.session.contador} veces!`);
  }
})

app.get('/login', (req, res)=>{
  const {username, password } = req.query;

  if(username !== 'coder' || password !== 'house'){
    return res.send('Login failed!');
  }
  req.session.user = username;
  req.session.admin = true;

  console.log('session: ',req.session);

  res.send('Login sucess!');
})

app.get('/privado', auth, (req, res)=>{
  res.send('Se encuentra logeado')
})

app.get('/logout', (req, res)=>{
  req.session.destroy(err=>{
    if (err) {
     res.json({err}) 
    } else{
      res.send('Logout ok');
    }
  })
})

/* ---------------------- Server ---------------------- */
const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`Servidor express escuchando en el puerto ${PORT}`);
});