import mongoose from 'mongoose';
import { usuarioModel } from './models/usuarios.model.js';
import { config } from './utils/config.js';

async function Main() {
    try {
        const strConn = `mongodb://${config.db.host}:${config.db.port}/${config.db.dbName}`
        const conn = await mongoose.connect(strConn, config.db.options);
        console.log('Base de datos conectada!');

        let response;
        //Insert Document
        const usr1 = {nombre: 'Juan', apellido: 'Perez', email: 'jp@email.com', usuario: 'jp', password: "123456"}
        response = await usuarioModel.create(usr1);
        console.log(`Documento insertado`, response);

        // Find Document
        response = await usuarioModel.find();
        console.log(`Documento Seleccionado`, response);

        //Update Document
        response = await usuarioModel.updateOne({nombre: "Juan"}, {$set: {password: "654321"}})
        console.log(`Documento Actualizado`, response);

        // //Update Document
        // response = await usuarioModel.updateMany({nombre: "Juan"}, {$set: {password: "654321"}})
        // console.log(`Documento Actualizado`, response);

        //Delete Document
        response = await usuarioModel.deleteOne({nombre: "Juan"})
        console.log(`Documento Eliminado`, response);
        // response = await usuarioModel.deleteMany({nombre: "Juan"})
        // console.log(`Documento Eliminado`, response);

        await mongoose.disconnect();
        console.log('Base de datos desconectada!');

    } catch (error) {
        console.log(error);
    }
}
Main();
