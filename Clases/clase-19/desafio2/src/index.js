import mongoose from "mongoose";
import { estudianteModel } from "./models/estudiante.model.js";
import { config } from './utils/config.js'

const strConn = `mongodb://${config.db.host}:${config.db.port}/${config.db.dbName}`;

async function Main() {
    try {
        const conn = await mongoose.connect(strConn, config.db.options);
        console.log('Base de datos conectada!');

        let response;

        response = await estudianteModel.find({}).sort({nombre: 1});
        console.log('\n1) Estudiantes ordenados por orden alfabético según sus nombres', response)

        response = await estudianteModel.find({}).sort({nombre: 1}).limit(1);
        console.log('\n2) El estudiante más joven', response);

        response = await estudianteModel.find({ curso: '2A' });
        console.log('\n3) Los estudiantes que pertenezcan al curso \'2A\'', response);

        response = await estudianteModel.find({}).sort({ edad: 1 }).skip(1).limit(1);
        console.log('\n4) El segundo estudiante más joven', response);

        response = await estudianteModel.find({}, { nombre: 1, apellido: 1, curso: 1, _id: 0 }).sort({ apellido: -1 })
        console.log('\n5) Sólo los nombres y apellidos de los estudiantes con su curso correspondiente, ordenados por apellido descendente (z a a)', response);
        
        response = await estudianteModel.find({ nota: 10 });
        console.log('\n6) El estudiante que sacó mejor nota', response);

        let sumNota = 0;
        response = await estudianteModel.find();
        response.forEach((estudiante)=>{
            sumNota += estudiante.nota;
        })
        console.log(`\n7) El promedio de notas del total de alumnos ${(sumNota/response.length).toFixed(2)}`);

        await estudianteModel.find({curso: '1A'},{},{}).exec((err, estudiantes) => {
            sumNota = 0;
            estudiantes.forEach((estudiante)=>{
                sumNota += estudiante.nota;
            })
            console.log(`\n8) El promedio de notas del curso \'1A\' ${(sumNota/estudiantes.length).toFixed(2)}`);
        });

        await mongoose.disconnect();
        console.log('Base de datos desconectada!'); 

    } catch (error) {
        console.log(error);
    }
}
Main();