/* ---------------------- Modulos ----------------------*/
const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');

const app = express();

/* ---------------------- Middlewares ---------------------- */
app.use(express.static(path.join(__dirname, 'public')));

//Motor de Plantillas
app.set('views', path.join(__dirname, 'views'));
app.engine('hbs', exphbs.engine({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: 'hbs'
}));
app.set('view engine', 'hbs');

/* ---------------------- Rutas ----------------------*/
app.get('/', (req, res)=>{
    res.send('Proyecto Handlebars ejecutandose!');
})

app.get('/saludo', (req, res)=>{
    res.render('saludo-general', {nombre: 'Gustavo Cerati', curso: 'Programacion Backend'});
});

app.get('/saludo2', (req, res)=>{
    res.render('saludo-general', {nombre: 'Luis Miguel', curso: 'Programacion Backend'});
});

app.get('/datos', (req, res)=>{
    res.render('datos-personales', {nombre: 'Luis Miguel', apellido: 'Gallego', edad: 52, email: 'luismi@email.com', telefono: 'N/D', curso: 'Programacion Backend'});
});

/* ---------------------- Servidor ----------------------*/
const PORT = 8081;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});