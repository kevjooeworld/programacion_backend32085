/* ---------------------- Modulos ----------------------*/
const express = require('express');
const path = require('path');
const fs = require('fs');

const app = express();
/* ---------------------- Middlewares ---------------------- */
app.use(express.urlencoded({ extended: true })); // Cuando interactua con formularios
app.use(express.static(path.join(__dirname, 'public')));

//motor de plantillas
app.engine('cte', async (filePath, options, callback) => {
    try {
        const content = await fs.promises.readFile(filePath);
        const rendered = content.toString()
                        .replace('^^titulo$$', '' + options.titulo + '')
                        .replace('^^mensaje$$', '' + options.mensaje + '')
                        .replace('^^autor$$', '' + options.autor + '')
                        .replace('^^version$$', '' + options.version + '');
        return callback(null, rendered);                
    } catch (error) {
        return callback(new Error(error));
    }
});

app.set('views', './views');
app.set('view engine', 'cte');

/* ---------------------- Rutas ----------------------*/
app.get('/cte', (req, res) => {
    console.log(req.query);
    const datos = { 
      titulo: req.query.titulo ,
      mensaje: req.query.mensaje,
      autor: req.query.autor,
      version: req.query.version
    }
  
    res.render('plantilla1', datos)
})

/* ---------------------- Servidor ----------------------*/
const PORT = 8082;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});

/*
Prueba
http://localhost:8082/cte?titulo=Prisionero%20de%20Azkaban&mensaje=Lorem%20ipsum
*/