/* ---------------------- Modulos ----------------------*/
const express = require('express');
const fs = require('fs');
/* ---------------------- Instancia de express ----------------------*/
const app = express();

/* ---------------------- Middlewares ---------------------- */
app.use(express.static('public'));
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

/* ---------------------- Conf. Motor de plantillas ---------------------- */
app.engine('cte', async (filePath, options, callback)=>{
    try {
        const contenido = await fs.promises.readFile(filePath);
        const htmlGenerado = contenido.toString()
                                    .replace('^^mensaje$$', options.mensaje);

        callback(null, htmlGenerado);
    } catch (error) {
        callback(new Error(error), null);
    }
});

/* ---------------------- Ruta de plantillas*/
app.set('views', './views');
/* ---------------------- conf extension*/
app.set('view engine', 'cte');

/* ---------------------- Rutas ----------------------*/
app.get('/ejemplo', (req, res)=>{
    const datos = {
        mensaje: 'Hola Coders! Creamos un custom Template Engine BD'
    }
    res.render('plantilla', datos);
});

/* ---------------------- Servidor ----------------------*/
const PORT = 7272;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto ${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});

/*
    npm init -y 
    npm install express body-parser express-handlebars
        "dev": "nodemon server.js"

*/