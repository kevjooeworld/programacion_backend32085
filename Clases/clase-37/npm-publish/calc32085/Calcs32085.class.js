export default class Calcs32085 {
    static suma(number1, number2){
        return Number(number1) + Number(number2);
    }
    
    static resta(number1, number2){
        return Number(number1) - Number(number2);
    }
    
    static multiplicacion(number1, number2){
        return Number(number1) * Number(number2);
    }
    
    static division(number1, number2){
        return Number(number1) / Number(number2);
    }
}