import Calcs32085 from '../main.js';

const a = 1;
const b = 2;

console.log('suma:', Calcs32085.suma(a, b));
console.log('resta:', Calcs32085.resta(a, b));
console.log('multiplicacion:', Calcs32085.multiplicacion(a, b));
console.log('division:', Calcs32085.division(a, b));