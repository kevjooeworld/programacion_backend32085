import { suma, resta, multiplicacion, division } from '../main.js';

const a = 1;
const b = 2;

console.log('suma:', suma(a, b));
console.log('resta:', resta(a, b));
console.log('multiplicacion:', multiplicacion(a, b));
console.log('division:', division(a, b));