import * as calcs32085 from '../main.js';

const a = 1;
const b = 2;

console.log('suma:', calcs32085.suma(a, b));
console.log('resta:', calcs32085.resta(a, b));
console.log('multiplicacion:', calcs32085.multiplicacion(a, b));
console.log('division:', calcs32085.division(a, b));