export function suma(number1, number2){
    return Number(number1) + Number(number2);
}

export function resta(number1, number2){
    return Number(number1) - Number(number2);
}

export function multiplicacion(number1, number2){
    return Number(number1) * Number(number2);
}

export function division(number1, number2){
    return Number(number1) / Number(number2);
}