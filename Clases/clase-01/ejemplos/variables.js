// Variables
let cambia = 1
const noCambia = 2

console.log('cambia',cambia) 
console.log('noCambia',noCambia) 

cambia = cambia + 10
console.log('cambia',cambia) 

// noCambia = 2
// console.log(noCambia)

const listaValores = [1, 2, 3]
console.log(listaValores)

listaValores.push(4)
console.log(listaValores)

listaValores = [1, 2, 3, 5, 6]
console.log(listaValores)

