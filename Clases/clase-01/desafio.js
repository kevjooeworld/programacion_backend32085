//1) Definir variables variables que almacenen los siguiente datos
const nombre = 'pepe';
let edad = 25;
let precio = 99.90;
const nombresSeriesFav = ['One Piece', 'Suits', 'GOT'];

const peliculasFav = [
    {
      nombre: 'volver al futuro I',
      anioEstreno: 1985,
      protagonistas: ['Christopher Lloyd', 'Michael J. Fox']
    }
];
  
  // 2
  console.log(nombre)
  console.log(edad)
  console.log(precio)
  console.log(nombresSeriesFav)
  console.log(peliculasFav)
  
  // 3
  edad++
  console.log(edad)
  
  // 4
  nombresSeriesFav.push('Attack on Titan')
  console.log(nombresSeriesFav)