/*
    Crear 4 funciones:
        - suma: de forma tradicional
        - resta: fx objeto, asigando a una variable
        - division: arrow fx, return normal
        - multiplicacion: arrow 1 line function
        - cuadrado: 1 line fx, sin parentesis
*/

function suma(a, b) {
    return a + b;
}

const resta = function (a, b) {
    return a - b;
}

const division = (a, b) => {
    return a/b;
}

const multiplicacion = (a, b) => a*b;

const cuadrado = a => a**2;

console.log(suma(1, 3));
console.log(resta(1, 3));
console.log(division(1, 3));
console.log(multiplicacion(1, 3));
console.log(cuadrado(3));