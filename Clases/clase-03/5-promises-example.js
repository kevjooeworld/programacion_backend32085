const dividir = (dividendo, divisor) => {
    return new Promise((resolve, reject) => {
        if(divisor == 0) {
            reject('No se puede dividir por cero (0)');
        } else {
            resolve(dividendo/ divisor);
       }    
    });
}

dividir(10, 2)
    .then((resultado)=>{
        console.log('Primer then')
        return resultado
    })
    .then((resultado)=>{
        return `resultado: ${resultado}`;
    })
    .then((resultadoTexto)=>{
        console.log('segundo then',resultadoTexto);
    })
    .catch((error)=> {
        console.log(`error ${error}`);
    })
    .finally(()=> {
        console.log('Final del la estructura ')
    });