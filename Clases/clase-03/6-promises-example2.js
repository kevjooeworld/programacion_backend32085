Promise.resolve(20)// Objeto Promise que devuelve/resuelve inmediantamente el valor 20
    .then( x => x + 1 )// entrada: 20, 20 + 1 = 21
    .then( x => x * 2 )// entrada: 21, 20 * 2 = 42
    .then( x => {
        if(x==22) throw 'Error' //false
        else return 80
    })
    .then( x => 30 )// entrada: 80, 30
    .then( x => x / 2 )// entrada: 30, 30 / 2 = 15
    .then( console.log )// imprime 15<<<<<<<<<<<<
    .catch( console.log )//No se ejecutara porque no hay caso con error 