const ejecutar = unaFuncion => unaFuncion();
const saludar = () => console.log('Saludos Coder!');

console.log(saludar())//Imprime solamente el retorno de la funcion
console.log(saludar)//Imprime el objeto "saludar"

ejecutar(saludar); //Forma correcta porque "saludar" es el objeto y definicion de la funcion como tal 
// ejecutar(saludar()); //Error: saludar() es una invocacion por lo que se recibiria su retorno