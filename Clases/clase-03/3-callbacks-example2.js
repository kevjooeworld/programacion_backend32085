/*
    Crear 4 funciones:
        - suma: de forma tradicional
        - resta: fx objeto, asigando a una variable
        - division: arrow fx, return normal
        - multiplicacion: arrow 1 line function
        - cuadrado: 1 line fx, sin parentesis
*/

const suma = (a, b) => a + b;
const resta = (a, b) => a - b;
const multiplicacion = (a, b) => a * b;
const division = (a, b) => a / b;

const operacion = (a, b, op) => op(a, b);

//Test 
console.log(operacion(1, 2, suma));
console.log(operacion(1, 2, resta));
console.log(operacion(1, 2, division));
console.log(operacion(1, 2, multiplicacion));