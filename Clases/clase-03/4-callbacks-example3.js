function escribirYLoguear(texto, callbackParaLoguear) {
    // simulamos que escribimos en un archivo!
    console.log(texto)
    // al finalizar, ejecutamos el callback
    callbackParaLoguear('archivo escrito con éxito')
}
   
escribirYLoguear('hola mundo de los callbacks!', (mensajeParaLoguear) => {
    const fecha = new Date().toLocaleDateString()
    console.log(`${fecha}: ${mensajeParaLoguear}`)
});

escribirYLoguear('Callbacks again!', (mensajeParaLoguear) => {
    let aparaciones = 2;
    console.log(`${aparaciones}: ${mensajeParaLoguear}`)
});

/* VERSION 2 */
function escribirYLoguearV2(texto, callbackParaLoguear) {
    
    console.log('> Se inicio el programa')
     // al finalizar, ejecutamos el callback
    console.log(`Se ejecutan las operaciones: ${texto}`)
    callbackParaLoguear(`log archivo (${texto})`)

    console.log('Finalizo el programa <')
}

// simulamos que escribimos en un archivo!
const logger = msg => {
    const fecha = new Date().toLocaleDateString()
    console.log(`${fecha}: ${msg}`)
}

escribirYLoguearV2('Configuracion de sistema', logger);