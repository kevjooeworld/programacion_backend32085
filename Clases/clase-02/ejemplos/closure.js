/*
    Crear una funcion que tenga la capacidad de crear sandwiches de un ingrediente principal Proteina (Pollo, Chorizo, Cerdo, Res)
    Que posea la capacidad de agregarle complementos sobre lo anterior
*/

function armarSanguche(ingredientePrincipal) {
    return function (complementos) {
        console.log(`El sanguche es de ${ingredientePrincipal} con complemento adicional de ${complementos}`);
    }    
}

const sangucheCarne = armarSanguche('Res');
sangucheCarne('jamon y queso');

const sangucheChorizo = armarSanguche('Chorizo');
sangucheChorizo('Chimi churry');


/*
    Crear una funcion que evalue si el resultado es el esperado
*/
function sum(par1, par2) {
    return par1 + par2;
}

function expect(actual) {
    return {
        toBe(expected) {
            if(actual !== expected) {
                console.log(`Error ${actual} no coincide con ${expected}`)
            } else {
                console.log('Todo bien!')
            }
        },
        other(){
            console.log(`En memoria tengo ${actual}`)
        }
    }
}

expect(sum(3, 2)).toBe(6);
expect(sum(3, 2)).other();

