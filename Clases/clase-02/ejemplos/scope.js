//scope global, es accesible desde cualquier punto del archivo
const USER_NAME = 'coder01';
const USER_TYPE = 'admin';

//scope dentro de bloques de programa

//scope de una function 
function myFn() {
    const USER_FN = '1';

    //acceso a variables globales
    console.log(`USER: ${USER_NAME} TYPE: ${USER_TYPE}`);

    //acceso al scope local
    console.log(`Fn: ${USER_FN}`);
}
myFn();

//scope de un ciclo de iteración
const listaDatos = [1, '1', 2, '2', 3, '3'];
for (let index = 0; index < listaDatos.length; index++) {
    const element = listaDatos[index];
    console.log(`index[${index}] = value(${element})`)
}
console.log(`Last index[${index}] = value(${element})`)

console.log('Acceso a variables de scope generales y especificos')
//acceso a variables globales
console.log(`USER: ${USER_NAME} TYPE: ${USER_TYPE}`);
//acceso al scope local de myFn
// console.log(`Fn: ${USER_FN}`);//LEVANTA ERROR