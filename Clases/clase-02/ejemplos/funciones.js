let billetera = 0.00;

//Funcion deposito 
function deposito(monto) {
    billetera = billetera + monto; 
}

const retiro = function (monto) {
    billetera = billetera - monto; 
}

/** Pruebas de las funciones */
console.log('Monto inicial billetera', billetera);

deposito(500.00);
console.log('Luego del deposito', billetera);

retiro(200);
console.log('Luego del retiro', billetera);
