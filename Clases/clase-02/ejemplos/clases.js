class Persona {

    static conVida = true;

    constructor(nombre, apellido, direccion) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
    }

    mostrarNombre(){
        return `${this.nombre} ${this.apellido}`;
    }

    obtenerConVida(){
        return Persona.conVida;
    }
}

const persona1 = new Persona('Alejandra', 'Herrera', 'Donde hay una calle al par de un arbol');
console.log(persona1);
console.log(persona1.mostrarNombre());
console.log(persona1.obtenerConVida());

const persona2 = new Persona('Jose', 'Torres', 'Donde hay una calle al par de un arbol x2');
console.log(persona2);
console.log(persona2.mostrarNombre());
console.log(persona2.obtenerConVida());

