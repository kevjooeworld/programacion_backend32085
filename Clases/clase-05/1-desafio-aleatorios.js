/*
    Cuantas veces se repite cada numero
    {
        "1": 200,
        "2": 324,
        "3": 301,
        .
        .
        .
        "20": 198
    }
*/

const listaOcurrencias = {};

function generarAletario(numInicial, numFinal) {
    let semilla = Math.random();
    let numAleatorio = parseInt(semilla * numFinal) + numInicial;
    // console.log(semilla, numAleatorio) //Para comprender el uso del random
    return numAleatorio;
}

function conteoAleatorios(numInicial, numFinal, cantidad) {
    let numAleatorio = 0;
    for (let conteo = 0; conteo < cantidad; conteo++) {
        numAleatorio = generarAletario(numInicial, numFinal);
        
        if (listaOcurrencias.hasOwnProperty(numAleatorio.toString())) {
            listaOcurrencias[numAleatorio.toString()] += 1;
        } else {
            listaOcurrencias[numAleatorio.toString()] = 1;
        }
    }
    return listaOcurrencias;
}

console.log(conteoAleatorios(1, 20, 100));