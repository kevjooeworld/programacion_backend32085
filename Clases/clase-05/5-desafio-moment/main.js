const moment = require('moment');

const hoy = moment();
const nacimiento = moment('29/11/2000', 'DD/MM/YYYY');

const edad = hoy.diff(nacimiento, 'years');
const difDays = hoy.diff(nacimiento, 'days');

console.log(`Hoy es ${hoy.format("DD/MM/YYYY")}`);
console.log(`Nací el ${nacimiento.format("DD/MM/YYYY")}`);
console.log(`Desde mi nacimiento han pasado ${edad} años.`)
console.log(`Desde mi nacimiento han pasado ${difDays} días.`);