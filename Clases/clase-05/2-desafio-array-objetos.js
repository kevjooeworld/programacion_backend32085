const productos = [
    { id:1, nombre:'Escuadra', precio:323.45 },
    { id:2, nombre:'Calculadora', precio:234.56 },
    { id:3, nombre:'Globo Terráqueo', precio:45.67 },
    { id:4, nombre:'Paleta Pintura', precio:456.78 },
    { id:5, nombre:'Reloj', precio:67.89 },
    { id:6, nombre:'Agenda', precio:78.90 }
]

function obtenerNombres(productos) {
    let nombres = productos.map((p)=> p.nombre);
    return nombres.join(', ');
}

function calcularPrecioTotal(productos) {
    let precios = productos.map((p)=> p.precio);
    let precioTotal = precios.reduce((a, b)=> a + b, 0);
    return precioTotal;
}

function calcularPrecioPromedio(productos) {
    let precioPromedio = calcularPrecioTotal(productos)/productos.length;
    return precioPromedio;
}

function obtnerMenorPrecio(productos) {
    let min = productos[0].precio;
    let prod = productos[0];

    for (const producto of productos) {
        if (producto.precio < min) {
            min = producto.precio
            prod = producto
        }
    }
    return prod;
}

function obtnerMayorPrecio(productos) {
    let max = productos[0].precio;
    let prod = productos[0];

    for (const producto of productos) {
        if (producto.precio > max) {
            min = producto.precio
            prod = producto
        }
    }
    return prod;
}

console.log(obtenerNombres(productos));
console.log('AR: ', calcularPrecioTotal(productos).toLocaleString('es-AR', { style: 'currency', currency: 'USD'}));
console.log('US: ', calcularPrecioTotal(productos).toLocaleString('en-US', { style: 'currency', currency: 'USD'}));
console.log('HN: ', calcularPrecioTotal(productos).toLocaleString('es-HN', { style: 'currency', currency: 'HNL'}));
console.log(calcularPrecioPromedio(productos).toLocaleString('es-AR', { style: 'currency', currency: 'USD'}));
console.log(obtnerMenorPrecio(productos));
console.log(obtnerMayorPrecio(productos));