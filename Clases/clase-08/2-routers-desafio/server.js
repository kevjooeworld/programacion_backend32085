/* ---------------------- Modulos ----------------------*/
const express = require('express');
const morgan = require('morgan');

//Instancia de Server
const app = express();
const routerMascotas = require('./src/routes/mascotas.routes.js');
const routerPersonas = require('./src/routes/personas.routes.js')

/* ---------------------- Middlewares ---------------------- */
app.use(express.json());
app.use(morgan('dev'));

/* ---------------------- Rutas ----------------------*/
app.use('/api/mascotas', routerMascotas);
app.use('/api/personas', routerPersonas);

/* ---------------------- Servidor ----------------------*/
const PORT = 8081;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto http://localhost:${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});