/* ---------------------- Modulos ----------------------*/
const express = require('express');
const { Router } = express;

//Instancia de Server
const app = express();

//Segmento 1 de rutas
const router1 = Router(); //Se crea el primer segmento de rutas
const router2 = Router(); //Se crea el segundo segmento de rutas


/* ---------------------- Rutas ----------------------*/
app.get('/', (req, res)=>{
    res.send('Hola Coders');
});

/* Router 1 - Endpoint /api/router1/   */
router1.get('/', (req, res)=>{
    res.send('Hola desde el segmento 1')
});

/* Router 1 - Endpoint /api/router1/recurso1   */
router1.get('/recurso1', (req, res)=>{
    res.send('Consumo ruta /recurso1 en router1')
});

/* Router 2 - Endpoint /api/router1/recurso1   */
router2.get('/', (req, res)=>{
    res.send('Consumo ruta base de router /api en router2')
});

router2.get('/recurso2', (req, res)=>{
    res.send('Consumo ruta /recurso2 en router2')
});

app.use('/api/router1', router1);
app.use('/api/router2', router2);

/* ---------------------- Servidor ----------------------*/
const PORT = 8081;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto http://localhost:${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});