/* ---------------------- Modulos ----------------------*/
const express = require('express');
const morgan = require('morgan');

//Instancia de Server
const app = express();
const routerMascotas = require('./src/routes/mascotas.routes.js');
const routerPersonas = require('./src/routes/personas.routes.js');

/* ---------------------- Middlewares ---------------------- */
app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));

app.use((req, res, next)=>{
    console.log(`Middleware 1 de App se ejecuta con todos`);
    next();
});

app.use((req, res, next)=>{
    console.log(`Middleware 2 de App se ejecuta con todos`);
    next();
});

/* ---------------------- Rutas ----------------------*/
app.use('/api/mascotas', routerMascotas);
app.use('/api/personas', routerPersonas);

app.get('/error', (req, res)=> {
    let x  = 1/0;
    res.status(200).send(x);
});

// Middleware de aplicacion error
app.use(function(err, req, res, next) {
    res.status(err.status || 500).send('Realice acciones para corregir el aerror!');
});

/* ---------------------- Servidor ----------------------*/
const PORT = 8081;
const server = app.listen(PORT, ()=>{
    console.log(`Servidor escuchando en puerto http://localhost:${PORT}`);
})
server.on('error', error=>{
    console.error(`Error en el servidor ${error}`);
});