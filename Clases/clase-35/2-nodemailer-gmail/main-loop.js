import { createTransport } from "nodemailer";
import dotenv from 'dotenv';
dotenv.config();

const TEST_EMAIL = process.env.EMAIL_ACCOUNT;
const PASS_EMAIL = process.env.EMAIL_PASSWORD;

console.log(`${TEST_EMAIL} ${PASS_EMAIL}`)

const emailTest = [
    'testexperiments900@gmail.com',
    'facuuni8@gmail.com', 
    'betofiorani@gmail.com', 
    'suecollarte@gmail.com', 
    'matiasromoli44@gmail.com', 
    'javiernedelcou@gmail.com',
    'santillanlautaro03@gmail.com'
]

const transporter = createTransport({
    service: 'gmail',
    port: 587,
    auth: {
        user: TEST_EMAIL,
        pass: PASS_EMAIL 
    }
});

for (const email of emailTest) {
    const emailContent = {
        from: 'NodeJS app <noreply@example.com>',
        to: `"Dear Developer!👩‍💻👨‍💻" ${email}`,
        subject: 'Les envio este meme! ',
        text: `Hello ${email}!`,
        html: '<html><style>div{padding:10px;}</style><body><h1>Colors can be set using color names</h1><div style="background-color:red"> <h1 style="color:white">Heading</h1></div><div style="background-color:yellow"> <h1 style="color:blue">Heading</h1></div><div style="background-color:blue"> <h1 style="color:yellow">Heading</h1></div></body></html>',
        attachments: [
            { path : './assets/meme.png'}
        ]
    }
    
    try {
        const info = await transporter.sendMail(emailContent);
        console.log(info);
    } catch (error) {
        console.log(error);
    }
}