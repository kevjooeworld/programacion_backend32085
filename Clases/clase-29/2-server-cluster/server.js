import express from 'express';
import cluster from 'cluster';
import os from 'os';

const app = express();
const CPU_CORES = os.cpus().length;


if (cluster.isPrimary) {
    console.log('Cant de cores: ', CPU_CORES);
    
    for (let i = 0; i < CPU_CORES; i++) {
        cluster.fork();
    }

    cluster.on('exit', worker => {
        console.log(`Worker ${process.pid} ${worker.id} ${worker.pid} finalizo ${new Date().toLocaleString()}`);
        cluster.fork();
    });

} else {
    const PORT = parseInt(process.argv[2]) || 8080;

    app.get('/', (req, res)=>{
        res.send(`Servidor express en ${PORT} - PID ${process.pid} - ${new Date().toLocaleString()}`)
    })

    app.listen(PORT, err => {
        if (!err) console.log(`Servidor express escuchando en el puerto ${PORT} - PID WORKER ${process.pid}`)
    });
}