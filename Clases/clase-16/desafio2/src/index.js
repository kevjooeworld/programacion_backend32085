const { options } = require('./utils/options');
const Modelo = require('./modelo')

const modeloArticulos = new Modelo(options); 

const articulos = [
    { nombre: 'Leche', codigo: 'AB-12', precio: 23.60, stock: 24 },
    { nombre: 'Harina', codigo: 'CD-34', precio: 12.80, stock: 45 },
    { nombre: 'DDL', codigo: 'EF-56', precio: 32.30, stock: 16 },
    { nombre: 'Fideos', codigo: 'FG-44', precio: 42.70, stock: 34 },
    { nombre: 'Crema', codigo: 'CR-77', precio: 67.90, stock: 24 }
]

const batch = async () =>{
    try {
        console.log("1) tabla creada");
        await modeloArticulos.crearTabla();

        console.log("2) registros insertados");
        await modeloArticulos.insertar(articulos);

        console.log("3) articulos seleccionados");
        let respuesta = await modeloArticulos.listar();
        console.table(respuesta);

        console.log("4) Elimina el articulo con id 3");
        await modeloArticulos.eliminar(3);

        console.log("5) Actualizar el stock a 0 del articulo con id = 2");
        await modeloArticulos.actualizar({id: 2}, {stock: 0});

        console.log("6) articulos seleccionados");
        respuesta = await modeloArticulos.listar();
        console.table(respuesta);

    } catch (error) {
        console.error(error);
    } finally {
        modeloArticulos.cerrarConexion();
    }
}

batch();