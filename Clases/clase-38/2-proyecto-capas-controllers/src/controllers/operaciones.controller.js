export async function getAllOperaciones(req, res) {
    res.status(200).json({
        status: 200,
        route: `${req.method} ${req.baseUrl} ${req.url}`
    });
}

export async function getOperacionById(req, res) {
    res.status(200).json({
        status: 200,
        route: `${req.method} ${req.baseUrl} ${req.url}`
    });
}

export async function postOperacion(req, res) {
    res.status(201).json({
        status: 200,
        route: `${req.method} ${req.baseUrl} ${req.url}`
    });
}

export async function putOperacion(req, res) {
    res.status(201).json({
        status: 200,
        route: `${req.method} ${req.baseUrl} ${req.url}`
    });
}

export async function deleteOperacion(req, res) {
    res.status(200).json({
        status: 200,
        route: `${req.method} ${req.baseUrl} ${req.url}`
    });
}