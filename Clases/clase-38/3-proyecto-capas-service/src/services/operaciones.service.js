const DB_OPERACIONES = [
    {
        tipo: 'suma',
        param1: 1,
        param2: 2,
        resultado: 3
    }
];

export async function obtenerOperaciones() {
    try {
        return DB_OPERACIONES;
    } catch (error) {
        throw new Error(`Error al obtener Operaciones`);
    }
}