import ContenedorMemoria from '../containers/Contenedor.memoria.js'

const container = new ContenedorMemoria();

export async function obtenerOperaciones() {
    try {
        return await container.listarAll();
    } catch (error) {
        throw new Error(`Error al obtener Operaciones`);
    }
}