import { Router } from "express";

const routerOperaciones = Router();

routerOperaciones.get('/', (req, res)=> res.send(`get all op`));
routerOperaciones.get('/:id', (req, res)=> res.send(`get op by id`));
routerOperaciones.post('/', (req, res)=> res.send(`post op`));
routerOperaciones.put('/', (req, res)=> res.send(`put op`));
routerOperaciones.delete('/', (req, res)=> res.send(`delete op`));

export default routerOperaciones;