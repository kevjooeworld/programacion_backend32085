import admin from "firebase-admin";
import serviceAccount from "./db/crt/clase20-740e5-firebase-adminsdk-bchy6-597ef75ddb.json" assert {type: "json"}

//Se concecta
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

CRUD();

async function CRUD() {
  try {
    //Conf modelo de collection a utilizar
    const db = admin.firestore();
    const usuarios =db.collection('usuarios');

    /* ------------------------------------------------------------------- */
    /*   Escritura de la base de datos collection: usuarios                */
    /* ------------------------------------------------------------------- */
    const usuariosList = [
      {nombre: 'Jose', dni: 11223344},
      {nombre: 'Ana', dni: 22334455},
      {nombre: 'Diego', dni: 33445566}
    ]

    for (const usuario of usuariosList) {
      let doc = usuarios.doc();//id generado automaticamente
      //let id = 1;
      //let doc = usuarios.doc(`${id}`); // id de forma manual
      await doc.create(usuario);
    }
    console.log('Usuarios insertados');

    /* ------------------------------------------------------------------- */
    /*   Lectura de la base de datos collection: usuarios                  */
    /* ------------------------------------------------------------------- */
    let response = await usuarios.get();
    let usariosres = response.docs.map((doc)=>({
      id: doc.id,
      nombre: doc.data().nombre,
      dni: doc.data().dni
    }))
    console.log('Usuarios recuperados', usariosres);

    /* ------------------------------------------------------------------- */
    /*   Lectura de la base de datos collection por id: usuarios           */
    /* ------------------------------------------------------------------- */
    let id = `SIczJm1THKISC6212VFo`;
    let doc = usuarios.doc(`${id}`);
    response = await doc.get();
    console.log('Consulta realizada', response.data())

    /* ------------------------------------------------------------------- */
    /*   Actualzacion de la base de datos collection: usuarios                  */
    /* ------------------------------------------------------------------- */
    id = `SIczJm1THKISC6212VFo`;
    doc = usuarios.doc(`${id}`);
    response = await doc.update({dni: '44332211'})
    console.log('usuario actualizado', response);

    /* ------------------------------------------------------------------- */
    /*   Delete de la base de datos collection: usuarios                  */
    /* ------------------------------------------------------------------- */
    id = `yyY2MFVVSBLix218SI5S`;
    doc = usuarios.doc(`${id}`);
    response = await doc.delete()
    console.log('usuario actualizado', response);

  } catch (error) {
    console.log(error);
  }  
}
