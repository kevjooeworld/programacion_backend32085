import {Schema, model} from "mongoose";

const usuarioSchema = Schema({
    nombre: {type: String,  require: true},
    apellido: {type: String,  require: true},
    dni: {type: String, require: true, unique: true}
});

export const usuarioModel = model('usuarios', usuarioSchema);