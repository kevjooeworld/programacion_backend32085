const express = require('express');
const Mensajes = require('./src/Mensajes.js')

const app = express();
const msg = new Mensajes();

app.get('/', (req, res)=>{
    // res.send('<h1 style="color: blue;">Bienvenidos al servidor express</h1>');
    res.send(msg.generarPlantillaMsg(2, 'cyan', 'Alberto'))
});

let vistas = 0;
app.get('/vistas', (req, res)=>{
    res.send(`cantidad de visitas es ${vistas++}`);
});

let fecha = new Date().toLocaleString();
app.get('/fyh', (req, res)=>{
    res.send(`fecha: ${fecha}`);
});

const PORT = 3000;
const server = app.listen(PORT, ()=>{
    console.log(`Server on http://localhost:${PORT}/`)
})