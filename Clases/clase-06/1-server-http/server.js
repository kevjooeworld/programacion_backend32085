const http = require('http');

const server = http.createServer((request, response)=>{
    switch (request.url) {
        case '/':
            response.end(`Este es nuestro servidor funcionando`);
            break;
        case '/coder':
            response.end(`Hola coders! Mi primer servidor http :D`);
            break;
        default:
            response.end(`404 - Page not found`);
            break;
    }
    // response.end(`Hola coders! Mi primer servidor http :D - Ruta: ${request.url}`);
})

const connectedServer = server.listen(8080, ()=>{
    console.log(`Servidor escuchando en http://localhost:8080/`)
})