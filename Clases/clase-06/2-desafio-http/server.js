const http = require('http');

const getMensaje = ()=> {
    // const fecha = new Date(2022, 8, 3, 22, 0, 0);
    // const fecha = new Date('2022-08-03T09:24:00');
    const fecha = new Date();//recibe la fecha actual
    console.log(fecha.toLocaleString());
    const hora = fecha.getHours();

    if (hora >= 6 && hora <= 12) {
        return 'Buenos dias!'
    } else if (hora >= 13 && hora <= 19) {
        return 'Buenas tardes!'
    } else if ((hora >= 20 && hora <= 23) || (hora >= 0 && hora <= 5)) {
        return 'Buenas noches!'
    }
}

const server = http.createServer((request, response) => {
    response.end(getMensaje());
});

const connectedServer = server.listen(8080, ()=> {
    console.log(`Servidor http escuchando en http://localhost:8080/`)
});