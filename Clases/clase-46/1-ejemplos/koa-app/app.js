import koa from 'koa';
import { koaBody } from 'koa-body';
import booksRouter from './src/books.routes.js';

const app = new koa();
app.use(koaBody());

app.use(booksRouter.routes());

app.listen(3000, ()=>{
    console.log('App running http://localhost:3000')
});