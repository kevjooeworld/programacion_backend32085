import Express from "express";
import { faker } from '@faker-js/faker'

const app = Express(); 

function generarObjetoRandom() {
    return {
        nombre: faker.name.firstName(),
        apellido: faker.name.lastName(),
        color: faker.color.human()
    }
}

app.get('/test', (req, res)=>{
    let objs = [];

    const cant = Number(req.query.cant);

    for (let index = 1; index <= cant; index++) {
        objs.push({id: index, ...generarObjetoRandom()})
    }

    res.status(200).json(objs);
})

const PORT = 8080;
const servidor = app.listen(PORT, ()=>{
    console.log(`Servidor Mock escuchando en el puerto: ${PORT}`);
});