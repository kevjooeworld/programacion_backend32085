import  express from "express";
import usuariosRouter from "./src/routes/usuarios.routes.js";

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/api/usuarios', usuariosRouter)

const PORT = 8080;
const servidor = app.listen(PORT, ()=>{
    console.log(`Servidor Mock escuchando en el puerto: ${PORT}`);
});