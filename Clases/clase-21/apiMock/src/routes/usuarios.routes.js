import { Router } from "express";
import { UsuarioMock } from "../mocks/usuario.mock.js";

const usuariosRouter = Router();
const apiUsuarios = new UsuarioMock()

usuariosRouter.get('/', (req, res)=>{
    res.status(200).json(apiUsuarios.listarAll())
})

usuariosRouter.post('/popular', (req, res)=>{
    const cant = req.query.cant;
    res.status(200).json(apiUsuarios.almacenar(apiUsuarios.generarDatos(cant)));
})

usuariosRouter.get('/:id', (req, res)=>{
    const id = req.params.id;
    res.status(200).json(apiUsuarios.listar(id));
})

usuariosRouter.put('/:id', (req, res)=>{
    res.status(200).send(apiUsuarios.actualizar(req.body));
})

usuariosRouter.delete('/:id', (req, res)=>{
    res.status(200).send(apiUsuarios.borrar(req.query.id));
})

export default usuariosRouter;