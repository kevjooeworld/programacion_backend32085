import axios from 'axios';
import { writeFile } from "fs";

const datos = await obtenerInfoSeleccion2('ARG');
await escribirResumen('res_asyncawait.json', datos);

async function obtenerInfoSeleccion(pais){
    const res = await axios.get(`https://worldcupjson.net/teams/${pais}`, {});
    return res.data;
}

async function obtenerInfoSeleccion2(pais){
    const res = await axios.get(`/teams/${pais}`, {
        baseURL: `https://worldcupjson.net`,
        headers: {
            'Content-Type':'application/json'
        }
    });
    return res.data;
}

async function escribirResumen(archivo, datos){
    writeFile(archivo, JSON.stringify(datos, null, '\t'), error => {
        if (error) throw new Error(`Error de escritura de archivo ${archivo}`)
        console.log(`Escritura ok de archivo ${archivo}`)
    })
}