import got from 'got';
import { writeFile } from "fs";

const datos = await obtenerInfoSeleccion('ARG');
await escribirResumen('res_asyncawait.json', datos);

async function obtenerInfoSeleccion(pais){
    const res = await got( 
    {
        url: `https://worldcupjson.net/teams/${pais}`,
        responseType: 'json'
    }
    );

    return res.body;
}

async function escribirResumen(archivo, datos){
    writeFile(archivo, JSON.stringify(datos, null, '\t'), error => {
        if (error) throw new Error(`Error de escritura de archivo ${archivo}`)
        console.log(`Escritura ok de archivo ${archivo}`)
    })
}