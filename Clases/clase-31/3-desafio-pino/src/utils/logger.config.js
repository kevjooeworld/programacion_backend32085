import pino from 'pino';
import { config } from './config.js';

function buildDefaulLogger() {
    const devLogger = pino({}, pino.multistream(
        [
            {stream: process.stdout},
            {stream: pino.destination('./debug.log')}
        ]
    ))
    devLogger.level = 'info';
    return devLogger
}

function buildProductionLogger() {
    const prodLogger = pino('error.log');
    prodLogger.level = 'warn'
    return prodLogger
}

let logger = null;

console.log(config.env)

if (config.env == 'production') {
    logger = buildProductionLogger();
} else {
    logger = buildDefaulLogger();
}

export {logger};
