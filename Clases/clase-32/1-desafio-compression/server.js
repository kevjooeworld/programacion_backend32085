import express from 'express';
import compression from 'compression';

const app = express();

function generarContenido(contenidoStr, cantRepeticiones) {
    return contenidoStr.repeat(cantRepeticiones);
}

app.get('/saludo', (req, res)=>{
    res.send(generarContenido('Hola que tal', 1000))
})

app.get('/saludo-zip', compression(),(req, res)=>{
    res.send(generarContenido('Hola que tal', 1000))
})

const PORT = parseInt(process.argv[2]) || 8080
app.listen(PORT, () => {
    console.log(`Servidor express escuchando en el puerto ${PORT}`)
});
