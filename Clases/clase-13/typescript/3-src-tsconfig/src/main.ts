import * as operaciones from "./operaciones";

const num1: number = 10;
const num2: number = 4;

console.log(`La suma ${num1} + ${num2} es ${operaciones.sumar(num1, num2)}`);
console.log(`La suma ${num1} - ${num2} es ${operaciones.resta(num1, num2)}`);