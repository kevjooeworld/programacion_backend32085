// export const sumar = (a: number, b: number): number => a + b;
// export const resta = (a: number, b: number): number => a - b;
// export const multiplicacion = (a: number, b: number): number => a * b;
// export const division = (a: number, b: number): number => a / b;

const sumar = (a: number, b: number): number => a + b;
const resta = (a: number, b: number): number => a - b;
const multiplicacion = (a: number, b: number): number => a * b;
const division = (a: number, b: number): number => a / b;

export { sumar, resta, multiplicacion, division };