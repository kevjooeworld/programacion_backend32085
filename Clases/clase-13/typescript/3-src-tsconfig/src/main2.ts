import { resta, sumar } from "./operaciones";

const num1: number = 10;
const num2: number = 4;

console.log(`La suma ${num1} + ${num2} es ${sumar(num1, num2)}`);
console.log(`La suma ${num1} - ${num2} es ${resta(num1, num2)}`);