"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var operaciones_1 = require("./operaciones");
var num1 = 10;
var num2 = 4;
console.log("La suma ".concat(num1, " + ").concat(num2, " es ").concat((0, operaciones_1.sumar)(num1, num2)));
console.log("La suma ".concat(num1, " - ").concat(num2, " es ").concat((0, operaciones_1.resta)(num1, num2)));
