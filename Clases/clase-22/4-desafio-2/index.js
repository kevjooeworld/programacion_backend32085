import { holding } from "./utils/data.js";
import { print } from "./utils/functions.js";
import { schema, normalize, denormalize } from 'normalizr'

const empleado = new schema.Entity('empleado');
const organigrama = new schema.Entity('organigrama', {
    gerente: empleado,
    encargado: empleado,
    empleados: [ empleado ]
});

const grupo = new schema.Entity('grupo', {
    empresas: [organigrama]
});

console.log(' ------------- OBJETO NORMALIZADO --------------- ')
const normalizedData = normalize(holding, grupo);
print(normalizedData) 

console.log(' ------------- OBJETO DESNORMALIZADO --------------- ')
const denormalizedData = denormalize(normalizedData.result, grupo, normalizedData.entities);
//print(denormalizedData)

const longO = JSON.stringify(holding).length
console.log('Longitud objeto original', longO);

const longN = JSON.stringify(normalizedData).length
console.log('Longitud objeto normalizado', longN);

const longD = JSON.stringify(denormalizedData).length
console.log('Longitud objeto denormalizado', longD);

const porcentaje = ((longN*100)/longO).toFixed(2)
console.log(`Porcentaje de compresion: ${porcentaje}%`)