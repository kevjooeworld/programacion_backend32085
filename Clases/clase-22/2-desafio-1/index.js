import { denormalize, normalize, schema } from "normalizr"
import { empresa } from './utils/data.js';
import { print } from "./utils/functions.js";

const empleado = new schema.Entity('empleado');

const organigrama = new schema.Entity('organigrama', {
    gerente: empleado,
    encargado: empleado,
    empleados: [ empleado ]
});

console.log(' ------------- OBJETO NORMALIZADO --------------- ')
const normalizedEmpresa = normalize(empresa, organigrama);
print(normalizedEmpresa)

console.log('Longitud objeto original: ', JSON.stringify(empresa).length)
console.log('Longitud objeto normalizado: ', JSON.stringify(normalizedEmpresa).length)

console.log(' ------------- OBJETO DESNORMALIZADO --------------- ')
const denormalizedEmpresa = denormalize(normalizedEmpresa.result, organigrama, normalizedEmpresa.entities);
print(denormalizedEmpresa)
console.log('Longitud objeto original: ', JSON.stringify(empresa).length)
console.log('Longitud objeto desnormalizado: ', JSON.stringify(denormalizedEmpresa).length)