import { schema, normalize, denormalize } from "normalizr";
import util from 'util';

const blogpost = {
    id: "1",
    title: "My blog post",
    description: "Short blogpost description",
    content: "Hello world",
    author: {
      id: "1",
      name: "John Doe"
    },
    comments: [
      {
        id: "1",
        author: "Rob",
        content: "Nice post!"
      },
      {
        id: "2",
        author: "Jane",
        content: "I totally agree with you!"
      }
    ]
   }

const authorSchema = new schema.Entity('authors');
const commentSchema = new schema.Entity('comments');

const postSchema = new schema.Entity('posts', {
    author: authorSchema,
    comments: [commentSchema]
})

const normalizedData = normalize(blogpost, postSchema);
console.log(
    util.inspect(normalizedData, false, 12, true),
    JSON.stringify(normalizedData).length
);

const denormalizedData = denormalize(normalizedData.result, postSchema, normalizedData.entities)
console.log(
    util.inspect(denormalizedData, false, 12, true),
    JSON.stringify(denormalizedData).length
);