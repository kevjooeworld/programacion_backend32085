import { schema, denormalize, normalize } from "normalizr"
import { originalData } from "./utils/data.js";
import { print } from "./utils/functions.js";

/* ---------------[Definicion de esquemas]---------------- */
const user = new schema.Entity('users');
const comment = new schema.Entity('comments', {
    commenter: user
});
const article = new schema.Entity('articles', {
    author: user,
    comments: [comment]
});
const posts = new schema.Entity('posts', {
    posts: [article]
});

console.log(`--------- Objeto Original ---------`);
console.log(JSON.stringify(originalData).length)

console.log(`--------- Objeto Normalizado ---------`);
const normalizedData = normalize(originalData, posts)
print(normalizedData);
console.log(JSON.stringify(normalizedData).length)

console.log(`--------- Objeto Denormalizado ---------`);
const denormalizedData = denormalize(normalizedData.result, posts, normalizedData.entities)
print(denormalizedData);
console.log(JSON.stringify(denormalizedData).length)
