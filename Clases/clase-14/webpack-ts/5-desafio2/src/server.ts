import express from "express";

import { Superficie } from "./lib/superficie";
import { Perimetro } from "./lib/perimetro";

const app = express();
const objPerimetro = new Perimetro();
const objSuperficie = new Superficie();

/* ---------------------- Middlewares ---------------------- */
app.use(express.static('public'));
app.use(express.json())
app.use(express.urlencoded({extended: true}));

app.get('/calculo/:figura', (req, res)=> {
    const {figura} = req.params;
    const {param1, param2} = req.query;

    let perimetro:number = 0;
    let superficie:number = 0;

    switch (figura) {
        case 'cuadrado':
            perimetro = objPerimetro.cuadrado(Number(param1));
            superficie = objSuperficie.cuadrado(Number(param1));
            break;
        case 'rectangulo':
            perimetro = objPerimetro.rectangulo(Number(param1), Number(param2));
            superficie = objSuperficie.rectangulo(Number(param1), Number(param2));
            break;
        case 'circulo':
            perimetro = objPerimetro.circulo(Number(param1));
            superficie = objSuperficie.circulo(Number(param1));
            break;
        default:
            res.status(404).json({method: req.url, msg: 'Ingrese una figura valida'})
            break;
    }

    res.status(200).json({
        calculo: 'perimetro',
        figura,
        param1,
        param2,
        perimetro,
        superficie
    })
})

app.get('*', (req, res) => {
    res.status(404).json(
        {codigo: 404, msg: 'Recurso no encontrado'}
    )
})

const PORT = 4000;
app.listen(PORT, ()=> {
    console.log(`Servidor escuchando en puerto ${PORT}`);
});

/*
    

    Testing Routes
    http://localhost:4000/calculo/cuadrado?param1=2
*/