import { a1 } from "./utils/a1.js";
import { a2 } from "./utils/a2.js";
import { a3 } from "./utils/a3.js";

function main() {
    a1();
    a2();
    a3();
}
main();