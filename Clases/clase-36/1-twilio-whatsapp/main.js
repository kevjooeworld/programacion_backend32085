import twilio from "twilio";
import dotenv from 'dotenv';
dotenv.config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;

const client = twilio(accountSid, authToken);

const numbers = ['+50499605855', '+56942589810', '+598858254', '+5401130637045', '+59898858254', '+573052215557', '+543434250860', '+541155922969'];

try {
    let message = '';

    for (const number of numbers) {
        message = await client.messages.create({
            from: 'whatsapp:+14155238886',
            to: `whatsapp:${number}`,
            body: 'Todo funciona bien con Twilio y NodeJS! Con la clase 36',
            mediaUrl: ['https://empresas.blogthinkbig.com/wp-content/uploads/2019/11/Imagen3-245003649.jpg?w=800']
        });

        console.log(message);
    }
} catch (error) {
    console.error(error);
}